#!/usr/bin/php
<?php

use jf\mime\Types;

require_once __DIR__ . '/../src/Types.php';

$types = Types::i();
$mimes = [
    'application/javascript'                  => 'js',
    'application/json'                        => 'json',
    'application/vnd.oasis.opendocument.text' => 'odt',
    'image/png'                               => 'png'
];
foreach ($mimes as $mime => $extension)
{
    assert($types->getExtension($mime) === $extension);
    assert($types->getMime($extension) === $mime);
}
assert($types->getMimeTypes() === json_decode(file_get_contents(__DIR__ . '/../src/mime-types.json'), TRUE));