<?php

namespace jf\mime;

/**
 * Gestiona la información de los tipos MIME.
 */
class Types
{
    /**
     * Ruta del archivo usado como base de datos.
     */
    public const DB_FILE = __DIR__ . '/mime-types.json';

    /**
     * Tipos MIME cargados.
     *
     * @var array
     */
    private static array $_mimeTypes = [];

    /**
     * Instancia usada como singleton.
     *
     * @var Types|null
     */
    private static ?Types $_instance = NULL;

    /**
     * Devuelve el primer tipo MIME para la extensión especificada.
     *
     * @param string $extension Extensión a buscar. Si tiene un punto al inicio se le quita.
     *                          Si se pasa un nombre de archivo se le extrae la extensión.
     *
     * @return string|NULL MIME asociado a la extensión o `NULL` si no se encontró la extensión.
     */
    public function getMime(string $extension) : ?string
    {
        $_mime = $this->getMimes($extension);

        return $_mime
            ? reset($_mime)
            : NULL;
    }

    /**
     * Devuelve todos los tipos MIME para la extensión especificada.
     *
     * @param string $extension Extensión a buscar. Si tiene un punto al inicio se le quita.
     *                          Si se pasa un nombre de archivo se le extrae la extensión.
     *
     * @return string[]|NULL Listado de tipo MIME asociado a la extensión o `NULL` si no se encontró la extensión.
     */
    public function getMimes(string $extension) : ?array
    {
        $_dot = strrpos($extension, '.');
        if ($_dot !== FALSE)
        {
            $extension = substr($extension, $_dot + 1);
        }

        return $this->getMimeTypes()[ $extension ] ?? NULL;
    }

    /**
     * Devuelve la extensión usada para el tipo de MIME especificado.
     *
     * @param string $mimeType Tipo MIME a buscar.
     *
     * @return string|NULL La extensión del tipo MIME o `NULL` si no se encontró el tipo.
     */
    public function getExtension(string $mimeType) : ?string
    {
        $_result = NULL;
        foreach ($this->getMimeTypes() as $_extension => $_mimeTypes)
        {
            if (in_array($mimeType, $_mimeTypes))
            {
                $_result = $_extension;
                break;
            }
        }

        return $_result;
    }

    /**
     * Devuelve el listado de todas las extensiones aceptadas con sus tipos MIME asociados.
     *
     * @return array
     */
    public function getMimeTypes() : array
    {
        if (!self::$_mimeTypes)
        {
            self::$_mimeTypes = json_decode(file_get_contents(self::DB_FILE), TRUE);
        }

        return self::$_mimeTypes;
    }

    /**
     * Devuelve la instancia a usar como singleton.
     *
     * @return static
     */
    public static function i() : static
    {
        if (!self::$_instance)
        {
            self::$_instance = new static();
        }

        return self::$_instance;
    }

    /**
     * Actualiza la base de datos desde la URL especificada y usando un callback para procesar cada línea.
     *
     * @param string   $url Ruta desde la cual leer el archivo con los tipos MIME.
     * @param callable $fn  Callback que procesa cada línea del archivo de entrada.
     *
     * @return void
     */
    private static function _parse(string $url, callable $fn) : void
    {
        $_mimes = &self::$_mimeTypes;
        foreach (file($url, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES) as $_line)
        {
            $_extensions = $fn(trim($_line));
            foreach ($_extensions as $_extension => $_mime)
            {
                $_current = $_mimes[ $_extension ] ?? [];
                if (!in_array($_mime, $_current))
                {
                    $_mimes[ $_extension ][] = $_mime;
                }
            }
        }
        foreach ($_mimes as $_extension => $_values)
        {
            sort($_mimes[ $_extension ]);
        }
        ksort($_mimes);
    }

    /**
     * Actualiza la base de datos desde diferentes fuentes de información.
     *
     * @return void
     */
    public static function update() : void
    {
        self::$_mimeTypes = [];
        static::updateFromApache();
        static::updateFromNginx();
        file_put_contents(
            self::DB_FILE,
            json_encode(self::$_mimeTypes, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
        );
    }

    /**
     * Actualiza la base de datos desde Apache.
     *
     * @param string $url Ruta desde la cual leer el archivo con los tipos MIME.
     *
     * @return void
     */
    public static function updateFromApache(string $url = 'http://svn.apache.org/repos/asf/httpd/httpd/trunk/docs/conf/mime.types') : void
    {
        self::_parse(
            $url,
            function ($line) : array
            {
                $_extensions = [];
                if ($line[0] !== '#')
                {
                    $_parts = preg_split('/\s+/', $line);
                    if (count($_parts) === 2)
                    {
                        $_extensions[ $_parts[1] ] = $_parts[0];
                    }
                }

                return $_extensions;
            }
        );
    }

    /**
     * Actualiza la base de datos desde Nginx.
     *
     * @param string $url Ruta desde la cual leer el archivo con los tipos MIME.
     */
    public static function updateFromNginx(string $url = 'http://hg.nginx.org/nginx/raw-file/default/conf/mime.types') : void
    {
        self::_parse(
            $url,
            function ($line) : array
            {
                $_extensions = [];
                if ($line[0] !== '#')
                {
                    if (preg_match('/^([^\s]+)\s*([^;]+);$/', trim($line), $_matchs))
                    {
                        $_mime = $_matchs[1];
                        foreach (preg_split('/\s+/', $_matchs[2]) as $_extension)
                        {
                            $_extensions[ $_extension ] = $_mime;
                        }
                    }
                }

                return $_extensions;
            }
        );
    }
}
